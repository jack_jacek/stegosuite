# crawlspider - main image downloaded
# B00083655 - Jacek Maniak
# ITB - Dublin 2018
import re

import scrapy
from gevent import os
from parsel import Selector
from scrapy import FormRequest, Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from stegoSuite import settings
from stegoSuite.items import ImageItem
from urlparse import urljoin, urlparse
import subprocess
import json
import sys
import csv

class ImageDownloader(CrawlSpider):

    # The name of the spider
    name = 'downloader'
    slinks = []
    simgs = []

    # initial parameters
    def __init__(self,
                 start_urls= '',
                 lUser = '',
                 lUserField = '',
                 lPass = '',
                 lPassField = '',
                 **kwargs):
        super(ImageDownloader, self).__init__(name=None, **kwargs)

        # get start URL
        self.start_urls = []
        self.start_urls.append(start_urls)
        print "Start_urls: _" + start_urls

        # get login details

        # get domain url from start_url
        parsed_uri = urlparse(start_urls)
        self.domain = '{uri.netloc}'.format(uri=parsed_uri)
        print "Domain: " + self.domain
        self.allowed_domains = []
        self.allowed_domains.append(self.domain)
        print self.allowed_domains

        # login details
        self.lUser = lUser
        self.lUserField = lUserField
        self.lPass = lPass
        self.lPassField = lPassField
        self.lUrl = ''

        self.file_name = ''
        self.total = 0

    @property
    def allowed_domains(self):
        return self._allowed_domains

    @allowed_domains.setter
    def allowed_domains(self, value):
        self._allowed_domains = value

    @property
    def start_urls(self):
        return self._start_urls

    @start_urls.setter
    def start_urls(self, value):
        self._start_urls = value

    # Method which starts the requests by visiting all URLs specified in start_urls
    def start_requests(self):
        print('START')
        print(self.allowed_domains)
        for url in self.start_urls:
            if (self.lUser) in '':
                yield scrapy.Request(url, callback=self.after_login, dont_filter=True)
            else:
                print "Logged as " + self.lUser
                # Loggin
                yield scrapy.Request(url, callback=self.login, dont_filter=True)

    # login to the site
    def login(self, response):
        return [FormRequest.from_response(response,
                    formdata={self.lUserField: self.lUser,
                              self.lPassField: self.lPass},
                    callback=self.after_login)]

    def after_login(self, response):
        print 'Lets start...'
        # The list of items that are found on the particular page
        items = []
        # # Only extract canonicalized and unique links (with respect to the current page)
        links = LinkExtractor(canonicalize=True, unique=True).extract_links(response)

        # # Now go through all the found links
        for link in links:
            # Check whether the domain of the URL of the link is allowed; so whether it is in one of the allowed domains
            is_allowed = False
            for allowed_domain in self.allowed_domains:
                if allowed_domain in link.url:
                    is_allowed = True

            # remove duplicates
            if link.url in str(self.slinks):
                is_allowed = False

            if is_allowed:
                imgs = response.xpath('//img').extract()
                print 'images'
                print imgs
                for img in imgs:
                    img_action = Selector(text=img).xpath('//img/@src').extract()
                    if  img_action not in self.simgs:
                        print img_action
                        self.simgs.append(img_action)
                item = ImageItem()
                item['url_to'] = link.url
                print link.url
                items.append(item)
                self.slinks.append(link.url)
            # Return all the found items
        #return items
        for link in links:
            #print link.url
            yield Request(link.url, callback=self.parse_attr)

    def parse_attr(self, response):
        item = ImageItem()
        item['url_to'] = response.url
        item['folder'] = self.domain
        imgs = response.xpath('//img').extract()
        self.total = self.total + len(imgs)

        for img in imgs:
            img_action = Selector(text=img).xpath('//img/@src').extract()
            print '>>>Image to download: ' + img_action[0]
            item['image_urls'] = urljoin(response.url, img_action[0])
            yield item

    def save(self, text):
        if self.file_name<>'':
            f = open(''.join(self.file_name),'a')
            f.write(text)
            f.close()

    def closed(self, reason):
        print self.total
        print('Done!')

        # run plugins - steganalysis
        if self.total>0:
            self.steganalysis()
            self.siteReport()

        sys.stdin.readline()

    def steganalysis(self):
        def fileParameter(value):
            if len(value)>0:
                if 'IMAGES_STORE' in value:
                    processList.append(settings.IMAGES_STORE+os.path.sep+self.domain)
                else:
                    processList.append(value)

        print '****** Plugins V1.0 ******'
        plugins = os.listdir('./plugins')
        for pluginName in plugins:
            print '****** ' + pluginName + ' ******'
            file = open('./plugins' + os.path.sep + pluginName, "r")
            # PART I - INPUTx
            processList = []
            regexValue =''
            for line in file:
                if '!MAIN_PROGRAM =' in line: processList.append(line.split("'")[1])
                if '!FILE_PARAMETER_1 =' in line: fileParameter(line.split("'")[1])
                if '!FILE_PARAMETER_2 =' in line: fileParameter(line.split("'")[1])
                if '!FILE_PARAMETER_3 =' in line: fileParameter(line.split("'")[1])
                if '!FILE_PARAMETER_4 =' in line: fileParameter(line.split("'")[1])
                if '!FILE_PARAMETER_5 =' in line: fileParameter(line.split("'")[1])
                if '!REGEX =' in line: regexValue = line.split("'")[1]
            photoPath = settings.IMAGES_STORE+os.path.sep+self.domain
            process = subprocess.Popen(processList, stdout=subprocess.PIPE)
            text = process.stdout.read()
            print '------------------------------------------------------------------------------------------------'
            print text
            print '------------------------------------------------------------------------------------------------'

            # PART II - OUTPUT
            # read stego analysis output
            for line in text.splitlines():
                # change to Regex
                fileNameRegex = re.search(regexValue, line)
                fileNameExt = fileNameRegex.group(0)
                # remove extension file
                fileName = fileNameExt[0:fileNameExt.find('.')]
                fullFileName = photoPath+os.path.sep+fileName+'.txt'
                # data structure
                data = {'Plugin': pluginName, 'Stego': True, 'Info': line}

                if os.path.exists(fullFileName):
                    append_write = 'a'  # append if already exists
                else:
                    append_write = 'w'  # make a new file if not

                with open(fullFileName, append_write) as outfile:
                    json.dump(data, outfile)
                    outfile.write('\n')
                print "stego: " + fileName
            print '..END of Analysis'

    def siteReport(self):
        print '---------------- Report -------------------'
        # set folder
        url = self.domain
        folderPath  = settings.IMAGES_STORE+os.path.sep+url
        csvFile = folderPath+os.path.sep+url+'.csv'
        myFile = open(csvFile, 'w')
        with myFile:
            myFields = ['url_address', 'file_name','file_type','file_size','stegano','plugins']
            writer = csv.DictWriter(myFile, fieldnames=myFields, lineterminator='\n')
            writer.writeheader()
            # Summary Folder (URL) Report
            files = os.listdir(folderPath)
            for file  in files:
                # only for images
                if '.txt' not in os.path.splitext(file)[1] and '.csv' not in os.path.splitext(file)[1]:
                    url_address = url
                    file_name = os.path.splitext(file)[0]
                    file_type = (os.path.splitext(file)[1])[1:]
                    file_size = str(os.path.getsize(folderPath+os.path.sep+file))
                    pathToReport = folderPath+os.path.sep+os.path.splitext(file)[0]+'.txt'
                    analysis = os.path.isfile(pathToReport)
                    if analysis:
                        stegano = 'True'
                    else:
                        stegano = 'False'
                    plugins = ""
                    try:
                        with open(pathToReport,'rU') as f:
                            for line in f:
                                data = json.loads(line)
                                plugins= plugins+(data['Plugin']+', ')
                    except:
                         plugins = ""
                    plugins = plugins[:-2]
                writer.writerow({
                    'url_address':url_address,
                    'file_name':file_name,
                    'file_type':file_type,
                    'file_size':file_size,
                    'stegano':stegano,
                    'plugins':plugins})

        print '---------------- END -------------------'

