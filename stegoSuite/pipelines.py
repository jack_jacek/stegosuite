# -*- coding: utf-8 -*-

# item pipelines
# B00083655 - Jacek Maniak
# ITB - Dublin 2018

import scrapy
from scrapy.contrib.pipeline.images import ImagesPipeline, ImageException, DropItem


class StegosuitePipeline(ImagesPipeline):
    # image downloader - media request
    def get_media_requests(self, item, info):
        print '>>>Image downloading: ' + item['image_urls']
        self.image_folder = item['folder']
        print "___FOLDER____: " + self.image_folder
        yield scrapy.Request(item['image_urls'])

    # save image
    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
        if not image_paths:
            raise DropItem("Item contains no images")
        item['image_paths'] = image_paths
        return item

    # get file name
    def file_path(self, request, response=None, info=None):
        image_guid = request.url.split('/')[-1]
        print '___image_guid___: ' + 'full/%s' % (image_guid)
        return self.image_folder+'/%s' % (image_guid)
