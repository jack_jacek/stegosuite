# main class
# B00083655 - Jacek Maniak
# ITB - Dublin 2018

import os
from PyQt5.QtCore import QDir, QSize
import threading
from PyQt5.QtGui import QIcon, QPixmap, QPalette, QBrush, QImage
from PyQt5.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QAction, QFileDialog, QDialog, QLabel
from PyQt5 import QtCore
from PyQt5.uic import loadUi
import os.path
import settings
import csv
from scanPage import ScanPage
import json

# Custom QFileSystemModel
# 4rd field added "Stego Status"

# Main Class - StegoSuite
from customSystemModel import CustomSystemModel


class MainWindow(QMainWindow):
    FROM, SUBJECT, DATE = range(3)
    threads = []

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        # Load the mainWindow ui
        loadUi('ui/mainWindow.ui', self)
        # Load initial mainWindow settings
        self.initUI()
        # show menu bar osX (bug-fix)
        self.menuBar.setNativeMenuBar(False)
        # menu bar buttons
        self.actionExit.triggered.connect(self.menuBarExit)
        self.actionFull_Report.triggered.connect(self.fullReport)
        self.actionAbout.triggered.connect(self.aboutStegoSuite)

        # background image
        oImage = QImage('resources/background.jpg')
        # resize Image to widgets size
        sImage = oImage.scaled(QSize(300,200))
        palette = QPalette()
        # 10 = Windowrole
        palette.setBrush(10, QBrush(sImage))
        self.setPalette(palette)

        # display all available plugins
        plugins = os.listdir('./plugins')
        self.newPlugin = []
        for plugin in plugins:
            #print plugin[:-5] # without .conf
            # Create new action
            self.newPlugin.append(QAction(plugin[:-5], self))
            #print len(self.newPlugin)-1
            self.menuPlugin.addAction(self.newPlugin[len(self.newPlugin)-1])

        self.pushButtonScan.clicked.connect(self.buttonEvent)
        self.checkTor.clicked.connect(self.checkedTorEvent)
        self.checkLogin.clicked.connect(self.checkedEvent)
        self.treeView.clicked.connect(self.treeEvent)

        # custom QFileSystemModel() -  add analysis status
        self.model = CustomSystemModel()

        # display only images formats
        self.model.setFilter(QDir.AllDirs | QDir.NoDotAndDotDot | QDir.AllEntries)
        self.model.setNameFilters(['*.jpg','*.png','*.bmp','*.gif'])
        self.model.setNameFilterDisables(False)

        # set image folder path
        self.model.setRootPath(settings.IMAGES_STORE)
        idx = self.model.index(settings.IMAGES_STORE)

        self.treeView.setModel(self.model)
        self.treeView.setRootIndex(idx)

        self.treeView.hideColumn(3)  # "Date modified" column
        self.treeView.setColumnWidth(0, 250)

    # test
    def newCall(self):
        print "Test"

    # close aplication
    def menuBarExit(self):
        QtCore.QCoreApplication.instance().quit()

    # get icon
    def initUI(self):
        self.setWindowTitle('Stego Suite')
        self.setWindowIcon(QIcon('resources/icons/stegoSuite.png'))
        self.show()

    def treeEvent(self, index):
        def fileReport(self, pathToReport):
            self.textBrowser.setText('File path: '+pathToReport)

            try:
                with open(pathToReport,'rU') as f:
                    for line in f:
                        data = json.loads(line)
                        self.textBrowser.append('Plugin: '+data['Plugin'])
                        self.textBrowser.append('Info: '+data['Info'])
            except:
                self.textBrowser.append('Error: file corrupted!')

        # get LSB bit
        def getBit(value):
            return ((value >> 0) & 1)

        def LSB(self):
            # display LSB view only for valid files
            if os.path.isfile(filePath):
                pixmap = QPixmap(filePath,"1")
                sceneLSB = QGraphicsScene()
                tmp = pixmap.toImage()
                for y in range(0,tmp.height()):
                    for x in range(0,tmp.width()):
                        color = tmp.pixelColor(x,y)
                        value = 128
                        red = value * getBit(color.red())
                        green = value * getBit(color.green())
                        blue = value * getBit(color.blue())
                        color.setRed(red)
                        color.setGreen(green)
                        color.setBlue(blue)
                        tmp.setPixelColor(x,y,color)
                    pixmapLSB = QPixmap.fromImage(tmp)
                sceneLSB.addPixmap(pixmapLSB)
                self.graphicsViewLSB.fitInView(sceneLSB.sceneRect(), QtCore.Qt.KeepAspectRatio)
                self.graphicsViewLSB.setScene(sceneLSB)

        # load preview image
        indexItem = self.model.index(index.row(), 0, index.parent())
        filePath = self.model.filePath(indexItem)
        pixmap = QPixmap(filePath,"1")
        scene = QGraphicsScene()
        scene.addPixmap(pixmap)
        self.graphicsView.fitInView(scene.sceneRect(), QtCore.Qt.KeepAspectRatio)
        self.graphicsView.setScene(scene)

        t = threading.Thread(target=LSB(self))
        self.threads.append(t)
        t.start()

        # read file report
        pathToReport = os.path.splitext(filePath)[0]+'.txt'
        analysis = os.path.isfile(pathToReport)
        if analysis:
            fileReport(self, pathToReport)
        else:
            self.textBrowser.setText("")

    def checkedTorEvent(self):
        # Checked Events
        if self.checkTor.isChecked() == True:
            print self.checkTor.text() + ' is selected'
        else:
            print self.checkTor.text() + ' is deselected'

    def checkedEvent(self):
        # Checked Events
        if self.checkLogin.isChecked() == True:
            print self.checkLogin.text() + ' is selected'
            self.editUser.setEnabled(True)
            self.editPass.setEnabled(True)
            self.editUserField.setEnabled(True)
            self.editPassField.setEnabled(True)
        else:
            print self.checkLogin.text() + ' is deselected'
            self.editUser.setEnabled(False)
            self.editPass.setEnabled(False)
            self.editUserField.setEnabled(False)
            self.editPassField.setEnabled(False)

    def buttonEvent(self):
        # Button Events
        scanPage = ScanPage()
        clicked = self.plainTextEdit1.text()
        print 'Button clicked: ' + clicked
        # get login details;
        if self.checkLogin.isChecked() == True:
            print self.checkLogin.text() + ' is selected'
            lUser = self.editUser.text()
            lUserField = self.editUserField.text()
            lPass = self.editPass.text()
            lPassField = self.editPassField.text()
        else:
            print self.checkLogin.text() + ' is deselected'
            lUser = ''
            lUserField = ''
            lPass = ''
            lPassField = ''

        print 'User: ' + lUser
        print 'UserField: ' + lUserField
        print 'Pass: ' + lPass
        print 'PassField: ' + lPassField
        scanPage.start(clicked, lUser, lUserField, lPass, lPassField)

    def aboutStegoSuite(self):
        print 'Analysis'
        dialog = QDialog()
        dialog.setFixedSize(762,466)
        dialog.setWindowTitle("About StegoSuite")
        # background image
        oImage = QImage('resources/about.png')
        # resize Image to widgets size
        sImage = oImage.scaled(QSize(762,466))
        palette = QPalette()
        # 10 = Windowrole
        palette.setBrush(10, QBrush(sImage))
        dialog.setPalette(palette)
        dialog.exec_()

    def fullReport(self):
        print '--------------- FULL REPORT ---------------'
        urls = os.listdir(settings.IMAGES_STORE)
        # open save dialog
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","",
                                                  "Tabular Data (*.csv)", options=options)
        if fileName:
            # set folder
            myFile = open(fileName+'.csv', 'w')
            with myFile:
                myFields = ['url_address', 'file_name','file_type','file_size','stegano','plugins']
                writer = csv.DictWriter(myFile, fieldnames=myFields, lineterminator='\n')
                writer.writeheader()
                for url in urls:
                    if '.DS_Store' not in url:
                        folderPath  = settings.IMAGES_STORE+os.path.sep+url
                        # Summary Folder (URL) Report
                        files = os.listdir(folderPath)
                        for file  in files:
                            # only for images
                            if '.txt' not in os.path.splitext(file)[1] and '.csv' not in os.path.splitext(file)[1]:
                                url_address = url
                                file_name = os.path.splitext(file)[0]
                                file_type = (os.path.splitext(file)[1])[1:]
                                file_size = str(os.path.getsize(folderPath+os.path.sep+file))
                                pathToReport = folderPath+os.path.sep+os.path.splitext(file)[0]+'.txt'
                                analysis = os.path.isfile(pathToReport)
                                if analysis:
                                    stegano = 'True'
                                else:
                                    stegano = 'False'

                                plugins = ""
                                try:
                                    with open(pathToReport,'rU') as f:
                                        for line in f:
                                            data = json.loads(line)
                                            plugins= plugins+(data['Plugin']+', ')
                                except:
                                     plugins = ""
                                plugins = plugins[:-2]
                            writer.writerow({
                                'url_address':url_address,
                                'file_name':file_name,
                                'file_type':file_type,
                                'file_size':file_size,
                                'stegano':stegano,
                                'plugins':plugins})

            print '- END --------- FULL REPORT ---------------'

if (__name__ == '__main__'):
    app = None

    if (not app):
        app = QApplication([])

    app.setWindowIcon(QIcon('resources/icons/stegoSuite.png'))
    window = MainWindow()
    window.show()

    if (app):
        app.exec_()
