# custom tree view
# B00083655 - Jacek Maniak
# ITB - Dublin 2018

from PyQt5.QtCore import QModelIndex
from PyQt5.QtWidgets import QFileSystemModel
from qtpy import QtCore
import os.path

class CustomSystemModel(QFileSystemModel):
    fileName = ""
    fileType = ""
    folderName = ""

    def columnCount(self, parent=QModelIndex()):
        return super(CustomSystemModel, self).columnCount() + 1

    def headerData(self, section, orientation, role):
        if section == self.columnCount() - 1:
            if role == QtCore.Qt.DisplayRole:
                return "Stego Status"
        return super(CustomSystemModel, self).headerData(section, orientation, role)

    def data(self, index, role):
        if index.column() == 0:
            if role == QtCore.Qt.DisplayRole:
                self.fileName = super(CustomSystemModel, self).data(index, role)

        if index.column() == 2:
            if role == QtCore.Qt.DisplayRole:
                self.fileType = super(CustomSystemModel, self).data(index, role)

        if index.column() == self.columnCount() - 1:
            if role == QtCore.Qt.DisplayRole:
                if "Folder" not in self.fileType:
                    # is the stegoanalysis report for the file?
                    pathToReport = os.path.splitext(QFileSystemModel.filePath(self, index))[0]+'.txt'
                    analysis = os.path.isfile(pathToReport)
                    if analysis:
                        return "found"
                    return ""

            if role == QtCore.Qt.TextAlignmentRole:
                return QtCore.Qt.AlignHCenter
        if index.column() !=4:
            return super(CustomSystemModel, self).data(index, role)
