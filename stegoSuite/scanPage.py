# spider manager - opens crawl processes
# B00083655 - Jacek Maniak
# ITB - Dublin 2018

from PyQt5.QtCore import QProcess

class ScanPage():
    def start(self, html_clicked, lUser, lUserField, lPass, lPassField):
        print 'ScanPage: ' + html_clicked
        command = 'scrapy'
        args = ['crawl', 'downloader',
                '-a', 'start_urls='+html_clicked,
                '-a', 'lUser='+lUser,
                '-a', 'lUserField='+lUserField,
                '-a', 'lPass='+lPass,
                '-a', 'lPassField='+lPassField]
        process = QProcess()
        process.startDetached(command, args)



