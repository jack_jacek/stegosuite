# -*- coding: utf-8 -*-

# Scrapy settings for stegoSuite project
# B00083655 - Jacek Maniak
# ITB - Dublin 2018

BOT_NAME = 'stegoSuite'

SPIDER_MODULES = ['stegoSuite.spiders']
NEWSPIDER_MODULE = 'stegoSuite.spiders'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 3

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'stegoSuite.pipelines.StegosuitePipeline': 300,
}

# Store photos location
#Windows
#IMAGES_STORE = 'C:\Users\jacekmaniak\stegoPhotos'
#Mac
IMAGES_STORE = '/Users/jacekmaniak/stegoPhotos'

# Enable debuging - log files
LOG_ENABLED = False
EXTENSIONS = {
   'scrapy.extensions.telnet.TelnetConsole': None
}
#LOG_STDOUT = True
#LOG_FILE = 'c:\scrapy_output.txt'
