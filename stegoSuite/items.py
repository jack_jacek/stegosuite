# -*- coding: utf-8 -*-

# models for scraped items
# B00083655 - Jacek Maniak
# ITB - Dublin 2018

import scrapy


class ImageItem(scrapy.Item):
    # scraped items - links
    url_from = scrapy.Field()
    url_to = scrapy.Field()
    url = scrapy.Field()

    # scraped items - images
    folder = scrapy.Field()
    image_urls = scrapy.Field()
    image_paths = scrapy.Field()
    images = scrapy.Field()


